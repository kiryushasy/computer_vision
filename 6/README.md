# Частотный словарь символов для изображения
<table>
  <tr>
    <td>Тема задания</td>
    <td>Цвет и текстура</td>
  </tr>
  <tr>
    <td>Срок дедлайна</td>
    <td>07.11.2022</td>
  </tr>
</table>
Определить сколько фигур каждого оттенка имеется на изображении. Программа должна выдавать общее количество фигур на изображении и отдельно для прямоугольников и кругов количество по оттенкам.<br>
![Balls](https://gitlab.com/kiryushasy/computer_vision/-/raw/master/6/balls_and_rects.png)