import cv2
import numpy as np
import time

cv2.namedWindow("Camera", cv2.WINDOW_FULLSCREEN)

cam = cv2.VideoCapture(0)
cam.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1)
cam.set(cv2.CAP_PROP_EXPOSURE, 2)

mask_lower = np.array([50, 90, 100])
mask_upper = np.array([130, 170, 150])

poits = [0, 0]
speed = 0 
time_start = round(time.time())

while cam.isOpened():
    ret, frame = cam.read()
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    mask_o = cv2.inRange(hsv, mask_lower, mask_upper)
    mask_o = cv2.erode(mask_o, None, iterations=2)
    mask_o = cv2.dilate(mask_o, None, iterations=2)
    contours = cv2.findContours(mask_o, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[0]
    if len(contours) > 0:
        c = max(contours, key=cv2.contourArea)
        (x,y), radius = cv2.minEnclosingCircle(c)
        if radius > 20:
            cv2.circle(frame, (int(x), int(y)), int(radius), (0,255,0),2)
            n_x = x - poits[0]
            n_y = y  - poits[1]
            r=250
            # distance = ((pow(n_x, 2)+pow(n_y,2))**0.5)/r
            distance = (n_x**2 + n_y **2) **0.5 / r
            time_stop = time.time()
            speed = distance / (time_stop - time_start)
            time_start = round(time.time())
            poits = [0, 0]
            cv2.putText(frame, f"Speed: {abs(round(speed/100, 1))} m/s", (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 4)
            
    
    
    key = cv2.waitKey(1)
    if key == ord('d'):
        break
    cv2.imshow("Camera", frame)
cam.release()
cv2.destroyAllWindows()