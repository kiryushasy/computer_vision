import cv2
import numpy as np
from skimage.measure import label

cap = cv2.VideoCapture("balls.mp4")

mask = np.ones((5,5))


cv2.namedWindow("Frames")
while cap.isOpened():
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_OTSU + cv2.THRESH_BINARY)
    
    thresh = cv2.erode(thresh, mask, iterations=1)
    thresh = cv2.dilate(thresh, mask, iterations=1)
    labeled = label(thresh)
    
    balls=0
    for i in range(1, labeled.max()+1):
        contours = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[0]
        if len(contours)> 0:
            c = max(contours, key=cv2.contourArea)
            (x,y), radius = cv2.minEnclosingCircle(c)
            if radius > 10:
                balls+=1
                cv2.drawContours(frame, contours, -1, (0,255,255) , 2)
    cv2.putText(frame, f"Ball: {balls}", (30, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0))  
     
    cv2.imshow("Frames", frame)
    key = cv2.waitKey(1)
    if key == ord('d'):
        break

cap.release()

cv2.destroyAllWindows()
