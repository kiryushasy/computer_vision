import cv2
import numpy as np
import random



def find_balls(frame, cl_low, cl_up):
    mask = cv2.inRange(hsv, cl_low, cl_up)
    mask = cv2.erode(mask, None,iterations=2)
    mask = cv2.dilate(mask, None,iterations=2)
    
    contours, _ = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if len(contours)>0:
        c = max(contours, key=cv2.contourArea)
        (x,y), radius = cv2.minEnclosingCircle(c)
        if radius >10:
            return ((int(x), int(y)), int(radius))
    return None

cv2.namedWindow("Camera", cv2.WINDOW_FULLSCREEN)

cam = cv2.VideoCapture(0)
cam.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1)
cam.set(cv2.CAP_PROP_EXPOSURE, -1)

sec_arr = ["orange", "blue", "yellow"]
random.shuffle(sec_arr)
print(f"Correct order ->{sec_arr}. Then press F. Check result in console")
color_arr ={
    "yellow":[np.array([10, 80, 210]), np.array([90,150,255])],
    "orange":[np.array([0,100,200]), np.array([50,170,255])],
    "blue":[np.array([98,120,170]), np.array([150,250,250])]
    }

while cam.isOpened():
    ret, frame = cam.read()
    key = cv2.waitKey(1)
    blured = cv2.GaussianBlur(frame, (11, 11), 0)
    hsv = cv2.cvtColor(blured, cv2.COLOR_BGR2HSV)
    
    find_ball = {}
    for color, (color_lower, color_upper) in color_arr.items():
        ball = find_balls(hsv,color_lower,color_upper)
        if ball != None:
            cv2.circle(frame, ball[0], ball[1], (0,255,255),2)
            find_ball[color] = int(ball[0][0])
    if len(find_ball) > 0:
        ans = []
        for i in sorted(find_ball, key=find_ball.get):
            ans.append(i)
    if key == ord("f"):
        if ans == sec_arr:
            print("true")
        else:
            print("false")
    elif key == ord('d'):
        break
    cv2.imshow("Camera", frame)
cam.release()
cv2.destroyAllWindows()
