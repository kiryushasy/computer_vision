import cv2
import numpy as np


img = cv2.imread("./task4.jpg")
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
blur = cv2.medianBlur(gray, 11)
thresh = cv2.threshold(blur, 0, 255, cv2.THRESH_OTSU)[1]

cnts = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
cnts = cnts[0]
for cnt in cnts:
    peri = cv2.arcLength(cnt, True)
    approx = cv2.approxPolyDP(cnt, 0.04 * peri, True)
    print(len(approx))
    if len(approx) > 7:
        ((x, y), r) = cv2.minEnclosingCircle(cnt)
        print("radius:", r)
        for p in approx:
            cv2.circle(img, (int(x), int(y)), (int(r)), (0,255,0),1)
cv2.namedWindow("frame", cv2.WINDOW_KEEPRATIO)
cv2.imshow("frame", img)

cv2.waitKey(0)
cv2.destroyAllWindows()

nom = 1.05714
dpi = 96

res1 = 2*r / dpi * 2.54 / nom
res2 = 2*r * nom
print("first result:", res1)
print("second result:", res2)




# gradazia serogo > porog
# porog > gistogramma. pervii pik - fon, vtoroi - object, porog - mejdy
# cv.THRESH_BINARY, cv.THRESH_OTSU[minimize razbros, chetkoe image]
# erode, dilate, сужение и расширение соответственно
# findContours, conturs can have point on conture
# approxPolyDp, points menshe
# analiz image:
# нахождение объекта по маске цвета, ploshad(contourArea), perimetr(arcLength)
# получение точек аппроксимации, получение координат объекта на изображении.
# BoundigBox – возвращает (x,y, ширина, высота)
# ConvexHull - вписанный выпуклый многоугольник