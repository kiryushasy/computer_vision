# import matplotlib.pyplot as plt
# import numpy as np
# from skimage.filters import sobel, threshold_otsu, threshold_local
# from skimage.measure import label, regionprops

# img = plt.imread("lama_on_moon.png")
# img = np.mean(img, 2)

# line = img.mean(0)
# diff = np.abs(np.diff(line))
# posx = np.where(diff> np.std(diff))[0]
# print(posx)

# line = img.mean(1)
# diff = np.abs(np.diff(line))
# posy = np.where(diff> np.std(diff))[0]
# print(posy)

# img = img[posy[0]:posy[1], posx[0]:posx[1]]

# thresh = threshold_local(img)
# img[img<thresh] = 0
# img[img>0] = 1

# sb = np.abs(sobel(img))
# thresh = threshold_otsu(sb)
# sb[sb<thresh] = 0
# sb[sb>0] = 1
# sb = np.abs(sb-1)

# lb = label(sb)
# props = regionprops(lb)
# areas = [prop.area for prop in props]
# sb[props[1].coords[:,0], props[1].coords[:,1]]=1

# result = np.zeros_like(sb)
# result[props[1].coords[:,0], props[1].coords[:,1]]=1


# plt.figure()
# plt.imshow(result)

# plt.figure()
# plt.subplot(121)
# plt.imshow(img)
# # plt.subplot(122)
# # plt.plot(diff)
# plt.subplot(122)
# plt.imshow(sb)
# plt.show()

import cv2
import matplotlib.pyplot as plt
cam = cv2.VideoCapture(0)
if cam.isOpened():
    ret, frame = cam.read()
    print(frame.shape)
cam.release()
plt.imshow(frame[:, :, ::-1])
plt.show()
