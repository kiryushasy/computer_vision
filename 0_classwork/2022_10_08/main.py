import matplotlib.pyplot as plt
import numpy as np
from scipy.misc import face


arr = np.array([[0,0,0,0,0,0,0,0,0,0],
                [0,0,0,0,0,0,0,0,0,0],
                [0,0,0,0,0,0,0,0,0,0],
                [1,1,1,1,1,1,1,1,1,1],
                [1,1,1,0,1,1,1,1,1,1],
                [1,1,1,1,1,1,1,1,1,1],
                [0,0,0,0,0,0,0,0,0,0],
                [0,0,0,0,0,0,0,0,0,0],
                [0,0,0,0,0,0,0,0,0,0],
                [0,0,0,0,0,0,0,0,0,0]])

def translation(image, vector):
    translated = np.zeros_like(image)
    for y in range(image.shape[0]):
        for x in range(image.shape[1]):
            ny = y + vector[0]
            nx = x + vector[1]
            if ny <0 or nx <0:
                continue
            if ny>=image.shape[0] or nx>=image.shape[1]:
                continue
            translated[ny,nx] = image[y,x]
    return translated

struct = np.ones((3,3))
struct[1, 1] = 1
struct[1, 1] = 1

def dilation(arr):
    result = np.zeros_like(arr)
    for y in range( 1, arr.shape[0]-1):
        for x in range(1, arr.shape[1]-1):
            rlog = np.logical_and(arr[y,x], struct)
            result[y-1:y+2, x-1:x+2] = np.logical_or(result[y-1:y+2, x-1:x+2], rlog)
    return result


def erosion(arr):
    result = np.zeros_like(arr)
    for y in range( 1, arr.shape[0]-1):
        for x in range(1, arr.shape[1]-1):
            sub = arr[y-1:y+2, x-1:x+2]
            if np.all(sub == struct):
                result[y,x] = 1
    return result    


def closing(arr):
    return erosion(dilation(arr))

result = closing(arr)

image = face(True)
translated = translation(image, [50,-50])


plt.subplot(121)
plt.imshow(arr)
plt.subplot(122)
plt.imshow(result)