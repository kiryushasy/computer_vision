import matplotlib.pyplot as plt
import numpy as np
from scipy.misc import face
from skimage.measure import label
from collections import defaultdict



def area(LB, label):
    return (LB == label).sum()




LB = np.zeros((16, 16))
LB[4:, :4] = 2

LB[3:10, 8:] = 1
LB[[3, 4, 3],[8, 8, 9]] = 0
LB[[8, 9, 9],[8, 8, 9]] = 0
LB[[3, 4, 3],[-2, -1, -1]] = 0
LB[[9, 8, 9],[-2, -1, -1]] = 0

LB[12:-1, 6:9] = 3
data = np.load("coins.npy")


lb = label(data)
nominals = np.array([1, 2, 5, 10])
areas = defaultdict(lambda: 0)

for lbl in range(1, lb.max()+1):
    areas[area(lb, lbl)] += 1

result = (np.array([areas[k] for k in sorted(areas)] 
                * nominals)).sum()
print(result)

# plt.imshow(image)