import cv2
import numpy as np

img = cv2.imread("frame.jpg")


# mask = np.ones((5,5))

converted = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

ret, thresh = cv2.threshold(converted, 120, 255, 0)

# th = cv2.erode(thresh, mask, iterations=2)
# th = cv2.erode(thresh, mask, iterations=2)
# cv2.drawContours(img, contours, -1, (0,255,0),1)
contours, _ = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
# cv2.drawContours(img, contours, 2, (0,255,0))
# print(len(contours))
# area = cv2.contourArea(contours[2], True)
# cv2.putText(img, str(area), (30,30), cv2.FONT_HERSHEY_SIMPLEX, 1, 255)
cv2.namedWindow("Frame", cv2.WINDOW_KEEPRATIO)
cv2.imshow("Frame",img)
cv2.waitKey(0)
cv2.destroyAllWindows()
    
